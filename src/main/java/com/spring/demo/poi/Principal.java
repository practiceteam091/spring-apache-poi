package com.spring.demo.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;


/**
 * Created by david on 10/5/17.
 */
public class Principal {
    public static void main(String[] args) throws IOException {
        System.out.println("start read excel");
        System.out.println(new File(".").getCanonicalPath());

        String excelFilePath="file2.xlsx";

        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));

        Workbook workbook = new XSSFWorkbook(inputStream);

        Sheet firstSheet = workbook.getSheetAt(0);

        Iterator <Row> iterator = firstSheet.iterator();

        while (iterator.hasNext()){
            Row nextRow = iterator.next();
            Iterator<Cell> cellIterator =nextRow.cellIterator();

            while (cellIterator.hasNext()){
                Cell cell = cellIterator.next();

                switch (cell.getCellType()){
                    case Cell.CELL_TYPE_STRING:
                        System.out.print(cell.getStringCellValue());
                }
                System.out.print("  -  ");

            }
            System.out.println();

        }
        workbook.close();
        inputStream.close();

    }
}
